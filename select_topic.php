<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Selección de Cluster</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
      background: url("./img/cluster/fondocluster.png") no-repeat center center fixed; 
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
      width: 100%;
      height: auto;
    }

  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <![endif]-->

      <!-- Fav and touch icons -->
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
      <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
      <link rel="shortcut icon" href="ico/favicon.png">
    </head>

    <body>
    <img class="bgimg" type="submit" src="./img/cluster/pregunta_02.png">
      <form method="POST" action="question.php" id="topicsForm">
        <input type="hidden" id="topic" name="topic" value="">
        <input type="hidden" name="username" id="username" value=<?php echo "\"".$_POST["username"]."\""; ?>>
        <table align="center" cellspacing="100">
          <tbody>
            <tr>
              <td align="center">
                <input src="./img/cluster/cluster_21.png" id="cluster2" 
                width="120" height="120" alt="cluster2" type="image" 
                onclick="sendTopic(2); sendUsername('question.php');">
              </td>
              <td align="center">
                <input src="./img/cluster/cluster_22.png" id="cluster4" 
                width="120" height="120" alt="cluster4" type="image" 
                onclick="sendTopic(4)">
              </td>
              <td align="center">
                <input src="./img/cluster/cluster_24.png" id="cluster3" 
                width="120" height="120" alt="cluster3" type="image" 
                onclick="sendTopic(3); sendUsername('question.php');">
              </td>
            </tr>
            <tr>
              <td align="center">
                <input src="./img/cluster/cluster_28.png" id="cluster1" 
                width="120" height="120" alt="cluster1" type="image" 
                onclick="sendTopic(1); sendUsername('question.php');">
              </td>
              <td align="center">
                <input src="./img/cluster/cluster_29.png" id="cluster5" 
                width="120" height="120" alt="cluster5" type="image" 
                onclick="sendTopic(5); sendUsername('question.php');">
              </td>
              <td align="center">
                <input src="./img/cluster/cluster_30.png" id="cluster6" 
                width="120" height="120" alt="cluster6" type="image" 
                onclick="sendTopic(6); sendUsername('question.php');">
              </td>
            </tr>
          </tbody>
        </table>
      </form>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/main.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap.min.js"></script>

  </body>
  </html>
