currentQuestion = 1;
var questionCount;
var correctAnswer;
var currentAnswer;
var score = 0;
var scoreBarPosition = 285;
var fiftyFifty = true;
var callAFriend = true;
var finish = false;
var count;

function loadWinners() {
    count = 1;
    $(document).ready(function() {
        $.ajax({
            type: "GET",
            url: "users.xml",
            dataType: "xml",
            success: function(xml) {
                $(xml).find('user').each(function() {
                    var name = $(this).attr('username');
                    var score = $(this).attr('score');
                    $('#tableBody').
                    append('<tr><td>' + count + '</td><td>' + name + '</td><td>' + score + '</td></tr>');
                    count++;
                });
            }
        });
    });
}

function randomWinner(){
    alert(getRandomInt(1, count-1));
}
function whoPlays(){
    alert(getRandomInt(1, 30));
}

function setUsername() {
    username = $("#username").val();
}

function hideAnswers(answer1, answer2) {
    $('#button_' + answer1).css("display", "none");
    $('#button_' + answer2).css("display", "none");
}

function showAnswers() {
    $('#button_a').css("display", "inline");
    $('#button_b').css("display", "inline");
    $('#button_c').css("display", "inline");
    $('#button_d').css("display", "inline");
}

function nextQuestion() {
    showAnswers();
    currentQuestion++;
    currentAnswer = 0;
    loadQuestion();
}

function clearSelectedQuestion() {
    $('#button_b').css("background-image", "url(./img/pregunta/button_b.png)");
    $('#button_c').css("background-image", "url(./img/pregunta/button_c.png)");
    $('#button_a').css("background-image", "url(./img/pregunta/button_a.png)");
    $('#button_d').css("background-image", "url(./img/pregunta/button_d.png)");
}

function hideResult() {
    if (finish) {
        window.location.href = "signin.html";
    }
    $('#correcto').css("display", "none");
    $('#incorrecto').css("display", "none");
    $('#continuar').css("display", "none");
}

function showResult(correcto) {
    if (correcto) {
        $('#correcto').css("display", "inline");
    } else {
        $('#incorrecto').css("display", "inline");
    }
    $('#continuar').css("display", "inline");
}

function selectQuestion(option) {
    //console.log("current: " + currentAnswer + " correct: " + correctAnswer);
    var imageUrl = "url(./img/pregunta/button_";
    if (currentAnswer == option) {
        if (currentAnswer == correctAnswer) {
            score++;
            $('#scoreBar').css("display", "inline");
            scoreBarPosition -= 36;
            $('#scoreBar').css("padding-top", scoreBarPosition + "px");
            showResult(true);
        } else {
            showResult(false);
        }
        clearSelectedQuestion();
        nextQuestion();
    } else {
        clearSelectedQuestion();
        currentAnswer = option;
        switch (option) {
            case 'a':
                imageUrl += "a_over.png)";
                $('#button_a').css("background-image", imageUrl);
                break;
            case 'b':
                imageUrl += "b_over.png)";
                $('#button_b').css("background-image", imageUrl);
                break;
            case 'c':
                imageUrl += "c_over.png)";
                $('#button_c').css("background-image", imageUrl);
                break;
            case 'd':
                imageUrl += "d_over.png)";
                $('#button_d').css("background-image", imageUrl);
                break;
            default:
                break;
        }
    }
}

function sendTopic(t) {
    $('#topic').val(t);
    $.post("question.php", {
        topic: t
    }, function(data) {});
}

function sendUsername(page) {
    var u = $('#username').val();
    $.post(page, {
        username: u
    }, function(data) {});
}


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function loadQuestion() {
    if (currentQuestion > questionCount) {
        saveScore();
    }
    $(document).ready(function() {
        $.ajax({
            type: "GET",
            url: "http://localhost/QQSM/questions.xml",
            dataType: "xml",
            success: function(xml) {
                var topic = $('#topic').val();
                $(xml).find('topic[id=\'' + topic + '\']').each(function() {
                    var questionnaireCount = $(this).find('questionnaire').length;
                    $(this).find('questionnaire[id=\'' +
                        getRandomInt(1, questionnaireCount) + '\']').each(function() {
                        // console.log("queestionaire: " + $(this).attr('id'));
                        questionCount = $(this).find('question').length;
                        var question = $(this).find('question[id=\'' + currentQuestion + '\']')
                        var text = $(question).attr('text');

                        //console.log(text + " currentQuestion: " + currentQuestion);
                        correctAnswer = $(question).attr('answer');

                        var a = $(question).find('answer[id=\'a\']').attr('text')
                        var b = $(question).find('answer[id=\'b\']').attr('text')
                        var c = $(question).find('answer[id=\'c\']').attr('text')
                        var d = $(question).find('answer[id=\'d\']').attr('text')
                        $("#questionText").html('');
                        $("#answer_a").html('');
                        $("#answer_b").html('');
                        $("#answer_c").html('');
                        $("#answer_d").html('');

                        $("#questionText").append(text);
                        $("#answer_a").append(a);
                        $("#answer_b").append(b);
                        $("#answer_c").append(c);
                        $("#answer_d").append(d);
                    });
                });
            }
        });
    });
}

function saveScore() {
    var realScore;
    switch (score) {
        case 0:
            realScore = 0;
            break;
        case 1:
            realScore = 10;
            break;
        case 2:
            realScore = 20;
            break;
        case 3:
            realScore = 30;
            break;
        case 4:
            realScore = 40;
            break;
        case 5:
            realScore = 50;
            break;
        case 6:
            realScore = 80;
            break;
        case 7:
            realScore = 100;
            break;
        case 8:
            realScore = 200;
            break;
        default:
            break;
    }
    showScore(realScore);
    $.get(
        "persistence.php", {
            username: $('#username').val(),
            score: realScore
        },
        function(data) {}
    );
    finish = true;
}

function showScore(realScore){
    hideResult();
    $('#scoreText').append(realScore);
    $('#scoreText').css("display", "inline");
    $('#puntaje').css("display", "inline");
    $('#continuar').css("display", "inline");
}

function deletePlayers(){
    $.get(
        "persistence.php", {
            delete: true
        },
        function(data) {}
    );
    alert("Todos los Jugadores han sido eliminados");
    winnersReload();
}

function winnersReload(){
    window.location.href = "winners.html";
}

function callAFriendHelp() {
    if (callAFriend) {
        alert("Puedes preguntarle a un amigo");
    }
    callAFriend = false;
    $('#callAFriend').attr("src", "./img/pregunta/boton_30over.png");
}

function fiftyFiftyHelp() {
    if (fiftyFifty) {
        var random = getRandomInt(1, 3);
        var answer1;
        var answer2;
        switch (correctAnswer) {
            case 'a':
                switch (random) {
                    case 1:
                        answer1 = 'b';
                        answer2 = 'c';
                        break;
                    case 2:
                        answer1 = 'c';
                        answer2 = 'd';
                        break;
                    case 3:
                        answer1 = 'd';
                        answer2 = 'b';
                        break;
                    default:
                        break;
                }
                break;
            case 'b':
                switch (random) {
                    case 1:
                        answer1 = 'a';
                        answer2 = 'c';
                        break;
                    case 2:
                        answer1 = 'c';
                        answer2 = 'd';
                        break;
                    case 3:
                        answer1 = 'd';
                        answer2 = 'a';
                        break;
                    default:
                        break;
                }
                break;
            case 'c':
                switch (random) {
                    case 1:
                        answer1 = 'a';
                        answer2 = 'b';
                        break;
                    case 2:
                        answer1 = 'a';
                        answer2 = 'd';
                        break;
                    case 3:
                        answer1 = 'b';
                        answer2 = 'd';
                        break;
                    default:
                        break;
                }
                break;
            case 'd':
                switch (random) {
                    case 1:
                        answer1 = 'a';
                        answer2 = 'b';
                        break;
                    case 2:
                        answer1 = 'b';
                        answer2 = 'c';
                        break;
                    case 3:
                        answer1 = 'a';
                        answer2 = 'c';
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
    $('#fifty_fifty').attr("src", "./img/pregunta/boton_35over.png");
    fiftyFifty = false;
    hideAnswers(answer1, answer2);
}