<?php
$path_XML = 'users.xml';
$username = $_GET['username'];
$score = $_GET['score'];
$delete = $_GET['delete'];

if($delete == null){
    try{
        if(!file_exists($path_XML)){
            $users = new SimpleXmlElement('<users/>');        
            $users->saveXML('users.xml');     
        }
        $users = new SimpleXMLElement($path_XML, null, true);
        $user = $users->addChild('user');
        $user->addAttribute("username", $username);
        $user->addAttribute("score", $score);

        header("Content-type: text/xml; charset=utf-8");
        echo $users->asXML();
        $users->asXML($path_XML);
    }catch(Exception $e){
        echo $e->getMessage();
    }
} else{
    unlink('users.xml');
}




?>