<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Cuestionario</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
  <!--link href="css/bootstrap.css" rel="stylesheet"-->
  <style type="text/css">

    .container {
      margin-left:auto; 
      margin-right:auto;
    }
    .table{
      position:absolute;
      top: 550px;
    }
    .center {
      margin-left:auto; 
      margin-right:auto;
    }

    body {
      padding-bottom: 40px;
      background: url("./img/pregunta/fondo.png") no-repeat center center fixed; 
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
      width: 100%;
      height: auto;
    }

    .bgimg {
      background-size: cover;
      width: 500px;
      height: 50px;
      text-align: center;
      vertical-align: center;
      padding: 0;
      border: none;
    }
    .question {
      position: relative;
      background-size: cover;
      width: 1340px;
      height: 50px;
      text-align: center;
      vertical-align: center;
      padding: 0;
      border: none;
      top: 145px;
      bottom: 396px;
      margin-left:auto; 
      margin-right:auto;
    }

    .questionFont{
     margin-left:30pt; 
     margin-right:30pt;
     margin-bottom:30pt;
     margin-top:30pt;
     max-width: 824px;
     height: auto;
   }
   .score{
    position:absolute;
    left: 1255px;
    right: 1310px;
    bottom: 392px;
    top: 42px;
    width: 60px;
    height: 300px;
    z-index: 1;
  }
  .result{
    display: none;
    position:absolute;
    z-index: 8;
    right: 200px;
    margin-left:auto; 
    margin-right:auto;
    margin-top: auto;
    margin-bottom: auto;
  }
  .continuar{
    display: none;
    position:absolute;
    z-index: 8;
    left: 550px;
    top: 517px;
    margin-left:auto; 
    margin-right:auto;
    margin-top: auto;
    margin-bottom: auto; }

    .scoreText{
    display: none;
    position:absolute;
    font-size: 50pt;
    text-align: center;
    z-index: 15;
    left: 600px;
    top: 426px;
    margin-left:auto; 
    margin-right:auto;
    margin-top: auto;
    margin-bottom: auto; }

</style>
<link href="css/bootstrap-responsive.css" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <![endif]-->

      <!-- Fav and touch icons -->
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
      <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
      <link rel="shortcut icon" href="ico/favicon.png">
    </head>

    <body onload="loadQuestion()" background="">

      <font align="right" style="padding-top: 50px; padding-left: 1220px" face="Arial" size='6pt'>Puntaje</font>
      <img class="score" src="./img/pregunta/barradepuntuacion_03.png" alt="barra de puntuacion">
      <img class="score "id="scoreBar" 
      style="display: none; width: 5%; height: 7%; rigth: 1226; left: 1228; padding-left: 0px; position:absolute; padding-top: 335px; z-index: 3;" 
      src="./img/pregunta/barradepuntaje_06.png" alt="barra de puntaje">

      <input src="./img/pregunta/boton_02.png" id="callAFriend"
      alt="call a friend" type="image" onclick="callAFriendHelp()" 
      style="position:absolute; width: 135px; height: 135px; top: 280px; bottom: 135px; left: 32; z-index: 4">

      <input src="./img/pregunta/boton_05.png" id="fifty_fifty" 
      alt="50/50" type="image" onclick="fiftyFiftyHelp()" 
      style="width: 135px; height: 135px; margin-top: 90px; left: 32;">

      <img id="incorrecto" class="result" src="./img/pregunta/incorrecto.png" alt="incorrecto">
      <img id="correcto" class="result" src="./img/pregunta/correcto.png" alt="correcto">
      <img id="puntaje" class="result" src="./img/pregunta/puntaje_02.png" alt="puntaje">
      <font color="gray" id="scoreText" class="scoreText" face="Arial"></font>
      <input id="continuar" class="continuar" src="./img/inicio/continuar.png" 
      width="15%" height="15%" alt="Continuar" type="image" onclick="hideResult()"> 

      

      <button align="center" class="question" type="submit" style="height:80pt; background-image: url('./img/pregunta/question_text.png')">
        <font align="center" class="questionFont" size="4.8pt" id="questionText" color="white">
        </font>
      </button>
      <div class="container" id="questionContainer">

        <input type="hidden" id="topic" value=<?php echo "\"".$_POST["topic"]."\""; ?>>
        <input type="hidden" name="username" id="username" value=<?php echo "\"".$_POST["username"]."\""; ?>>

        <table class="table" align="center" cellspacing="20">
          <tbody>
            <tr>
              <td align="center">
                <button id="button_a" class="bgimg" type="submit" 
                style="background-image: url('./img/pregunta/button_a.png')"
                onclick="selectQuestion('a')">
                <font id="answer_a"class="questionFont" color="gray"></font>
              </button>
            </td>
            <td align="center">
              <button id="button_c" class="bgimg" type="submit" 
              style="background-image: url('./img/pregunta/button_c.png')"
              onclick="selectQuestion('c')">
              <font id="answer_c"class="questionFont" color="gray"></font>
            </button>
          </td>
        </tr>
        <tr>
          <td align="center">
            <button id="button_b" class="bgimg" type="submit" 
            style="background-image: url('./img/pregunta/button_b.png')"
            onclick="selectQuestion('b')">
            <font id="answer_b"class="questionFont" color="gray"></font>
          </button>
        </td>
        <td align="center">
          <button id="button_d" class="bgimg" type="submit" 
          style="background-image: url('./img/pregunta/button_d.png')"
          onclick="selectQuestion('d')">
          <font id="answer_d"class="questionFont" color="gray"></font>
        </button>
      </td>
    </tr>
  </tbody>
</table>
</div> <!-- /container -->

<!-- Le javascript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="js/main.js"></script>
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/bootstrap.min.js"></script>

</body>
</html>
